<?php

function consultant_form_system_theme_settings_alter(&$form, &$form_state){
	$form['consultant_settings'] = array(
      '#type' => 'details',
      '#title' => t('Consultant Settings'),
      '#group' => 'innovation_theme_settings',
      '#weight' => -2
	);
	$form['consultant_settings']['preloader'] = array(
      '#type' => 'select',
      '#title' => t('Enable Preloader'),
      '#options' => array(
		  ''=>t('Disable'), 
		  'preloader'=>t('Enable'), 
	  ),
      '#default_value' => theme_get_setting('preloader'),
	);
	$form['consultant_settings']['gototop'] = array(
      '#type' => 'select',
      '#title' => t('Show Scroll to top button'),
      '#options' => array(
		  1=>t('Yes'), 
		  0=>t('No')
	  ),
      '#default_value' => theme_get_setting('gototop'),
	);
}

