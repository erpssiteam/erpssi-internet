/**
 * removePreloader
 * runSlider
 * fixHeight
 * goTop
 */
jQuery(function($){
   "use strict";
	function detectIE() {
		if (navigator.appName == 'Microsoft Internet Explorer'){
			return true; // IE
		}
		else if(navigator.appName == "Netscape"){
			return navigator.appVersion.indexOf('Edge') > -1; // EDGE
		}

		return false;
	}
	var removePreloader = function() {
		setTimeout(function() {
			$('.preloader').css({ 'opacity': 0, 'visibility':'hidden' });
		}, 1000);
		$('.preloader-toppart').addClass('floatOutTop');
		$('.preloader-bottompart').addClass('floatOutBottom');
		$(".preloader").delay(1000).fadeOut(10);

	};
	var runSlider = function() {
		if ($.isFunction($.fn.revolution)) {
			$('.tp-banner').show().revolution({
				dottedOverlay: "none",
				delay: 6000,
				startwidth: 1170,
				startheight: 700,
				hideThumbs: "on",

				thumbWidth: 100,
				thumbHeight: 50,
				thumbAmount: 5,

				navigationArrows: "solo",
				navigationStyle: "preview4",

				touchenabled: "on",
				onHoverStop: "on",

				swipe_velocity: 0.7,
				swipe_min_touches: 1,
				swipe_max_touches: 1,
				drag_block_vertical: false,

				parallax: "mouse",
				parallaxBgFreeze: "on",
				parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

				keyboardNavigation: "off",

				navigationHAlign: "center",
				navigationVAlign: "bottom",
				navigationHOffset: 0,
				navigationVOffset: 20,

				soloArrowLeftHalign: "left",
				soloArrowLeftValign: "center",
				soloArrowLeftHOffset: 20,
				soloArrowLeftVOffset: 0,

				soloArrowRightHalign: "right",
				soloArrowRightValign: "center",
				soloArrowRightHOffset: 20,
				soloArrowRightVOffset: 0,

				shadow: 0,
				fullWidth: "on",
				fullScreen: "off",

				spinner: "spinner4",

				stopLoop: "off",
				stopAfterLoops: -1,
				stopAtSlide: -1,

				shuffle: "off",

				autoHeight: "off",
				forceFullWidth: "off",

				hideThumbsOnMobile: "off",
				hideNavDelayOnMobile: 1500,
				hideBulletsOnMobile: "off",
				hideArrowsOnMobile: "off",
				hideThumbsUnderResolution: 0,

				hideSliderAtLimit: 0,
				hideCaptionAtLimit: 0,
				hideAllCaptionAtLilmit: 0,
				startWithSlide: 0,
				fullScreenOffsetContainer: ""
			});
		}
		;
	}
	var fixHeight = function(img,text) {
		if ( matchMedia('only screen and (min-width: 768px)').matches ) {
			$('body').imagesLoaded( function() {
				if ( $(img).length && $(text).length ) {
					$(img).height($(text).outerHeight());
				}
			});
		}
	};

	$(document).ready(function() {
		if (detectIE()) {
			$("body").addClass('ie');
		}
		removePreloader();
		runSlider();
		$(window).resize(function() {
			fixHeight('.feature-box .image','.feature-box .text');
		});
		$(window).on('load', function(event) {
			fixHeight('.feature-box .image','.feature-box .text');
		});
		/*Go to top*/
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#go-to-top').css({
					bottom: "10px"
				});
			} else {
				$('#go-to-top').css({
					bottom: "-100px"
				});
			}
		});
		$('#go-to-top').click(function () {
			$('html, body').animate({
				scrollTop: '0px'
			}, 800);
			return false;
		});
		if ( $.isFunction($.fn.magnificPopup) ) {
			$('.popup').each(function (index, element) {
				$(this).click(function () {
					$(this).parent().parent().siblings(".mf-popup").find(".mf-no-gallery a").click();
					$(this).parent().parent().siblings(".mf-popup").find(".carousel .carousel-inner a:first").click();
					return false;
				});
			});
			$(".magnific-popup, a[data-rel^='magnific-popup'],.mf-no-gallery a").magnificPopup({
				type: 'image',
				mainClass: 'mfp-with-zoom', // this class is for CSS animation below
				gallery: {
					enabled: true
				},
				zoom: {
					enabled: true,
					duration: 300,
					easing: 'ease-in-out',
					// The "opener" function should return the element from which popup will be zoomed in
					// and to which popup will be scaled down
					// By defailt it looks for an image tag:
					opener: function(openerElement) {
						// openerElement is the element on which popup was initialized, in this case its <a> tag
						// you don't need to add "opener" option if this code matches your needs, it's defailt one.
						return openerElement.is('img') ? openerElement : openerElement.find('img');
					}
				}

			});

			$('.mf-popup').find('.carousel .carousel-inner').once('mfp-processed').each(function() {
				$(this).magnificPopup({
					delegate: 'a',
					type: 'image',

					gallery: {
						enabled: true
					},
					removalDelay: 500,
					callbacks: {
						beforeOpen: function() {
							this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							this.st.mainClass = /*this.st.el.attr('data-effect')*/ "mfp-zoom-in";
						}
					},
					closeOnContentClick: true,
					// allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source
					midClick: true ,
					retina: {
						ratio: 1,
						replaceSrc: function(item, ratio) {
							return item.src.replace(/\.\w+$/, function(m) { return '@2x' + m; });
						}
					}

				});
			});
		}
	});
});

