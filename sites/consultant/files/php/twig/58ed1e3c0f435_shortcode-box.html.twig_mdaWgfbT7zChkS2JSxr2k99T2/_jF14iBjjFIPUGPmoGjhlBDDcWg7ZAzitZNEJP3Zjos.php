<?php

/* modules/inv_shortcodes/templates/shortcode-box.html.twig */
class __TwigTemplate_d3370c46a66987d2c6437ba6ce011d01e12958d73787ad4184b78d7cf34472d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 2);
        $filters = array("raw" => 18);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array('raw'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["class"]) ? $context["class"] : null), "html", null, true));
        echo "\">
    ";
        // line 2
        if ( !twig_test_empty((isset($context["image"]) ? $context["image"] : null))) {
            // line 3
            echo "       <div class=\"image-box\">
\t\t<img alt=\"\" src=\"";
            // line 4
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["image"]) ? $context["image"] : null), "html", null, true));
            echo " \">
\t   </div>
    ";
        }
        // line 7
        echo "\t";
        if ( !twig_test_empty((isset($context["icon"]) ? $context["icon"] : null))) {
            // line 8
            echo "       <div class=\"icon-box\">
\t\t<i class=\"";
            // line 9
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["icon"]) ? $context["icon"] : null), "html", null, true));
            echo "\"></i>
\t   </div>
    ";
        }
        // line 12
        echo "    <div class=\"box-content-wrapper\">
    ";
        // line 13
        if ( !twig_test_empty((isset($context["title"]) ? $context["title"] : null))) {
            // line 14
            echo "        <h3 class=\"box-title\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
            echo "</h3>
    ";
        }
        // line 16
        echo "    ";
        if ( !twig_test_empty((isset($context["content"]) ? $context["content"] : null))) {
            // line 17
            echo "        <div class=\"box-content\">
            ";
            // line 18
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["content"]) ? $context["content"] : null)));
            echo "
        </div>
    ";
        }
        // line 21
        echo "\t";
        if ( !twig_test_empty((isset($context["link_text"]) ? $context["link_text"] : null))) {
            // line 22
            echo "\t\t<div class=\"more-link\">
\t\t\t<a href=\"";
            // line 23
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["link"]) ? $context["link"] : null), "html", null, true));
            echo "\" class=\"read-more\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["link_text"]) ? $context["link_text"] : null), "html", null, true));
            echo "</a>
\t\t</div>
\t";
        }
        // line 26
        echo "    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/inv_shortcodes/templates/shortcode-box.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 26,  100 => 23,  97 => 22,  94 => 21,  88 => 18,  85 => 17,  82 => 16,  76 => 14,  74 => 13,  71 => 12,  65 => 9,  62 => 8,  59 => 7,  53 => 4,  50 => 3,  48 => 2,  43 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"{{ class }}\">
    {% if image is not empty %}
       <div class=\"image-box\">
\t\t<img alt=\"\" src=\"{{ image }} \">
\t   </div>
    {% endif %}
\t{% if icon is not empty %}
       <div class=\"icon-box\">
\t\t<i class=\"{{ icon }}\"></i>
\t   </div>
    {% endif %}
    <div class=\"box-content-wrapper\">
    {% if  title is not empty %}
        <h3 class=\"box-title\">{{ title }}</h3>
    {% endif %}
    {% if  content is not empty %}
        <div class=\"box-content\">
            {{ content | raw }}
        </div>
    {% endif %}
\t{% if link_text is not empty %}
\t\t<div class=\"more-link\">
\t\t\t<a href=\"{{ link }}\" class=\"read-more\">{{ link_text }}</a>
\t\t</div>
\t{% endif %}
    </div>
</div>";
    }
}
