<?php

/* modules/inv_quicksettings/templates/quicksettings.html.twig */
class __TwigTemplate_5f4084f6fbc5661b72818b672507e9d3e7aa6ccb6b7f8067db87bee7dc11ea6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("for" => 20);
        $filters = array("trans" => 13);
        $functions = array("path" => 22);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('for'),
                array('trans'),
                array('path')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 11
        echo "<span class=\"quicksettings_toggle fa fa-gears\"></span>
<div class=\"inv_settings\">
  <h3>";
        // line 13
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Layout Style")));
        echo "</h3>
  <select class=\"inv_layout form-select\" name=\"setting_layout\">
    <option value=\"wide\">Wide</option>
    <option value=\"boxed\">Boxed</option>
  </select>
  <h3>";
        // line 18
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Color")));
        echo "</h3>
    <ul class=\"presets\">
    ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["presets"]) ? $context["presets"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["preset"]) {
            // line 21
            echo "        <li class=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["preset"], "key", array()), "html", null, true));
            echo "\">
            <a href=\"";
            // line 22
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('drupal_core')->getPath("innovation.preset.setting", array("preset" => $this->getAttribute($context["loop"], "index", array()))), "html", null, true));
            echo "\"><span style=\"background-color: ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["preset"], "base_color", array()), "html", null, true));
            echo "\"> </span></a></li>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "    </ul>
  <h3>";
        // line 25
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Direction")));
        echo "</h3>
  <select class=\"inv_direction form-select\" name=\"setting_direction\">
    <option value=\"ltr\">LTR</option>
    <option value=\"rtl\">RTL</option>
  </select>
 <div class=\"clearfix\"></div>
</div>
\t\t\t\t\t\t\t\t\t";
    }

    public function getTemplateName()
    {
        return "modules/inv_quicksettings/templates/quicksettings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 25,  100 => 24,  82 => 22,  77 => 21,  60 => 20,  55 => 18,  47 => 13,  43 => 11,);
    }

    public function getSource()
    {
        return "{#
/**
 * @file
 * Default theme implementation for the basic structure of a inv_quicksettings.
 *
 * Available variables:
 * - presets: List of color which theme support
 * @ingroup themeable
 */
#}
<span class=\"quicksettings_toggle fa fa-gears\"></span>
<div class=\"inv_settings\">
  <h3>{{ 'Layout Style'|trans }}</h3>
  <select class=\"inv_layout form-select\" name=\"setting_layout\">
    <option value=\"wide\">Wide</option>
    <option value=\"boxed\">Boxed</option>
  </select>
  <h3>{{ 'Color'|trans }}</h3>
    <ul class=\"presets\">
    {% for preset in presets %}
        <li class=\"{{ preset.key }}\">
            <a href=\"{{ path('innovation.preset.setting', {'preset': loop.index}) }}\"><span style=\"background-color: {{ preset.base_color}}\"> </span></a></li>
    {% endfor %}
    </ul>
  <h3>{{ 'Direction'|trans }}</h3>
  <select class=\"inv_direction form-select\" name=\"setting_direction\">
    <option value=\"ltr\">LTR</option>
    <option value=\"rtl\">RTL</option>
  </select>
 <div class=\"clearfix\"></div>
</div>
\t\t\t\t\t\t\t\t\t";
    }
}
