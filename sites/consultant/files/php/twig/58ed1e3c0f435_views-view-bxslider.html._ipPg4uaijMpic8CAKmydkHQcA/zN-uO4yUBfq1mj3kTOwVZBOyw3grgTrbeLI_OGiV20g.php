<?php

/* modules/inv_views_bxslider/templates/views-view-bxslider.html.twig */
class __TwigTemplate_a8f0650333f003993111afc77ed1b816e1a8c1b0bea2dbbb5993079e7f562953 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("for" => 2, "set" => 8);
        $filters = array("length" => 2);
        $functions = array("range" => 2);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('for', 'set'),
                array('length'),
                array('range')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<ul id=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["view_id"]) ? $context["view_id"] : null), "html", null, true));
        echo "\" class=\"inv-bxslider\">
\t";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["rows"]) ? $context["rows"] : null)) - 1), (isset($context["num_rows"]) ? $context["num_rows"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 3
            echo "\t\t<li class=\"bxslide\">
\t\t\t";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range($context["i"], (($context["i"] + (isset($context["num_rows"]) ? $context["num_rows"] : null)) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                // line 5
                echo "\t\t\t\t";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["rows"]) ? $context["rows"] : null), $context["j"], array(), "array"), "html", null, true));
                echo "
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "\t\t</li>
\t    ";
            // line 8
            $context["i"] = ($context["i"] + (isset($context["num_rows"]) ? $context["num_rows"] : null));
            echo "\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "modules/inv_views_bxslider/templates/views-view-bxslider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 10,  71 => 8,  68 => 7,  59 => 5,  55 => 4,  52 => 3,  48 => 2,  43 => 1,);
    }

    public function getSource()
    {
        return "<ul id=\"{{view_id}}\" class=\"inv-bxslider\">
\t{% for i in range(0,rows|length - 1, num_rows) %}
\t\t<li class=\"bxslide\">
\t\t\t{% for j in i .. i + num_rows - 1 %}
\t\t\t\t{{ rows[j] }}
\t\t\t{% endfor %}
\t\t</li>
\t    {% set i = i + num_rows  %}\t\t
\t{% endfor %}
</ul>";
    }
}
