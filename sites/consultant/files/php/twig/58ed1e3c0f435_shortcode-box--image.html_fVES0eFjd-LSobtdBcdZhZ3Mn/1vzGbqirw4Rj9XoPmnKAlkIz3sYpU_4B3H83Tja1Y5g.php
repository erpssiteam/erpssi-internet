<?php

/* themes/consultant/templates/shortcode/shortcode-box--image.html.twig */
class __TwigTemplate_2a9b3122d517ca16d3b979e433ab71e4ba322baa5ae418f31bf89d59ebaeabe2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 2);
        $filters = array("raw" => 13, "t" => 22);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array('raw', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["class"]) ? $context["class"] : null), "html", null, true));
        echo "\">
\t";
        // line 2
        if ( !twig_test_empty((isset($context["image"]) ? $context["image"] : null))) {
            // line 3
            echo "\t   <div class=\"image-box\">
\t\t\t<img alt=\"";
            // line 4
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
            echo "\" src=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["image"]) ? $context["image"] : null), "html", null, true));
            echo " \">
\t   </div>
\t";
        }
        // line 7
        echo "\t";
        if ( !twig_test_empty((isset($context["icon"]) ? $context["icon"] : null))) {
            // line 8
            echo "\t   <div class=\"icon-box\">
\t\t\t<i class=\"";
            // line 9
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["icon"]) ? $context["icon"] : null), "html", null, true));
            echo "\"></i>
\t   </div>
\t";
        }
        // line 12
        echo "\t";
        if ( !twig_test_empty((isset($context["title"]) ? $context["title"] : null))) {
            // line 13
            echo "\t\t<h3><a href=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["link"]) ? $context["link"] : null), "html", null, true));
            echo "\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["title"]) ? $context["title"] : null)));
            echo "</a></h3>
\t";
        }
        // line 15
        echo "\t";
        if ( !twig_test_empty((isset($context["content"]) ? $context["content"] : null))) {
            // line 16
            echo "\t\t<div class=\"content-box-wrapper\">
\t\t\t<div class=\"content-box\">
\t\t\t\t";
            // line 18
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["content"]) ? $context["content"] : null)));
            echo "
\t\t\t</div>
\t\t";
        }
        // line 21
        echo "\t\t";
        if ( !twig_test_empty((isset($context["link_text"]) ? $context["link_text"] : null))) {
            // line 22
            echo "\t\t\t<div class='readmore-box'><a href='";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["link"]) ? $context["link"] : null), "html", null, true));
            echo "'><span>";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t((isset($context["link_text"]) ? $context["link_text"] : null))));
            echo "</span></a></div>
\t\t</div>
\t";
        }
        // line 25
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "themes/consultant/templates/shortcode/shortcode-box--image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 25,  100 => 22,  97 => 21,  91 => 18,  87 => 16,  84 => 15,  76 => 13,  73 => 12,  67 => 9,  64 => 8,  61 => 7,  53 => 4,  50 => 3,  48 => 2,  43 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"{{ class }}\">
\t{% if image is not empty %}
\t   <div class=\"image-box\">
\t\t\t<img alt=\"{{ title }}\" src=\"{{ image }} \">
\t   </div>
\t{% endif %}
\t{% if icon is not empty %}
\t   <div class=\"icon-box\">
\t\t\t<i class=\"{{ icon }}\"></i>
\t   </div>
\t{% endif %}
\t{% if title is not empty %}
\t\t<h3><a href=\"{{ link }}\">{{ title|raw }}</a></h3>
\t{% endif %}
\t{% if content is not empty %}
\t\t<div class=\"content-box-wrapper\">
\t\t\t<div class=\"content-box\">
\t\t\t\t{{ content | raw }}
\t\t\t</div>
\t\t{% endif %}
\t\t{% if  link_text is not empty %}
\t\t\t<div class='readmore-box'><a href='{{ link }}'><span>{{ link_text|t }}</span></a></div>
\t\t</div>
\t{% endif %}
</div>";
    }
}
