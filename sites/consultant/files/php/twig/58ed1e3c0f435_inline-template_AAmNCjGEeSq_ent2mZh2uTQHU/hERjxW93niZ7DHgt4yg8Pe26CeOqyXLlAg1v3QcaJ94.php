<?php

/* {# inline_template_start #}<div class="text">
 {{ body }}
 <span class="name">{{ title }}</span>
 <span class="position">{{ field_testimonial_position }}</span>
</div> */
class __TwigTemplate_ee1c84e82e246b098c66bf0bc1ecfa430cc9ac2220176ac4450d700ecd109d42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"text\">
 ";
        // line 2
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["body"]) ? $context["body"] : null), "html", null, true));
        echo "
 <span class=\"name\">";
        // line 3
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
        echo "</span>
 <span class=\"position\">";
        // line 4
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["field_testimonial_position"]) ? $context["field_testimonial_position"] : null), "html", null, true));
        echo "</span>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"text\">
 {{ body }}
 <span class=\"name\">{{ title }}</span>
 <span class=\"position\">{{ field_testimonial_position }}</span>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 4,  54 => 3,  50 => 2,  47 => 1,);
    }

    public function getSource()
    {
        return "{# inline_template_start #}<div class=\"text\">
 {{ body }}
 <span class=\"name\">{{ title }}</span>
 <span class=\"position\">{{ field_testimonial_position }}</span>
</div>";
    }
}
