<?php

/* themes/consultant/templates/shortcode/shortcode-accordion.html.twig */
class __TwigTemplate_35cc0e7a879ac48b7f6d185f45c40dbbbfe7062cdddaf6da1c40a5cbd50bfd29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 15
        echo "
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h4 class=\"panel-title\">
            <a class=\"";
        // line 19
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["item_collapse"]) ? $context["item_collapse"] : null), "html", null, true));
        echo "\" data-parent=\"#ACCORDION_WRAPPER_ID\" data-toggle=\"collapse\" href=\"#";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["accordion_item_id"]) ? $context["accordion_item_id"] : null), "html", null, true));
        echo "\">
                <i class='";
        // line 20
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["icon"]) ? $context["icon"] : null), "html", null, true));
        echo "'></i>";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
        echo "
                <i class=\"tm-toggle-button pull-right\"></i>
            </a>
        </h4>
    </div>
    <div class=\"panel-collapse collapse ";
        // line 25
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["item_active"]) ? $context["item_active"] : null), "html", null, true));
        echo "\" id=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["accordion_item_id"]) ? $context["accordion_item_id"] : null), "html", null, true));
        echo "\">
        <div class=\"panel-body\">";
        // line 26
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true));
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/consultant/templates/shortcode/shortcode-accordion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 26,  65 => 25,  55 => 20,  49 => 19,  43 => 15,);
    }

    public function getSource()
    {
        return "{#
/**
 * @file
 * Default theme implementation for accordion item.
 *
 * Available variables:
 * - accordion_item_id
 * - item_collapse
 * - item_active
 * - title
 * - icon
 * - content
 */
#}

<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h4 class=\"panel-title\">
            <a class=\"{{ item_collapse }}\" data-parent=\"#ACCORDION_WRAPPER_ID\" data-toggle=\"collapse\" href=\"#{{ accordion_item_id }}\">
                <i class='{{ icon }}'></i>{{ title }}
                <i class=\"tm-toggle-button pull-right\"></i>
            </a>
        </h4>
    </div>
    <div class=\"panel-collapse collapse {{ item_active }}\" id=\"{{ accordion_item_id }}\">
        <div class=\"panel-body\">{{ content }}
        </div>
    </div>
</div>
";
    }
}
