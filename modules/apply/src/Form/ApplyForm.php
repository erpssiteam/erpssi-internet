<?php

namespace Drupal\apply\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;

class ApplyForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'apply_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        /*
         * On recupere lid en GET si il y en a un
         */
        $id = \Drupal::request()->get('id');

        /* On recherche tous les postes*/
        $nids = \Drupal::entityQuery('node')->condition('type','les_postes_a_pourvoir')->execute();
        $articles =  \Drupal\node\Entity\Node::loadMultiple($nids);

        $list_offre = [null => ''];
        /*
         * On affilie un nom pour la liste déroulante
         */
        foreach($articles as $key => $article) {
            /* @var Node $article */
            $list_offre[$article->get('field_id_poste')->getValue()[0]['value']] = $article->get('title')->getValue()[0]['value'];
        }

        if(isset($list_offre)) {

            /*
             * On set l'id si il existe et on mets à disposition tous les champs du formulaire
             */
            $form['id_offre'] = array(
                '#type' => 'select',
                '#title' => ('Offre'),
                '#options' => $list_offre,
                '#default_value' => $id != null ? $id : null,
                '#required' => TRUE
            );

            $form['sexe'] = array(
                '#type' => 'select',
                '#title' => ('Sexe'),
                '#options' => array(
                    'femme' => t('Femme'),
                    'homme' => t('Homme'),
                ),
            );

            $form['lastname'] = array(
                '#type' => 'textfield',
                '#title' => t('Nom'),
                '#required' => TRUE,
            );

            $form['firstname'] = array(
                '#type' => 'textfield',
                '#title' => t('Prénom'),
                '#required' => TRUE,
            );

            $form['address'] = array(
                '#type' => 'textfield',
                '#title' => t('Adresse'),
                '#required' => TRUE,
            );

            $form['zip_code'] = array(
                '#type' => 'textfield',
                '#title' => t('Code Postal'),
                '#required' => TRUE,
            );

            $form['city'] = array(
                '#type' => 'textfield',
                '#title' => t('Ville'),
                '#required' => TRUE,
            );

            $form['pays'] = array(
                '#type' => 'textfield',
                '#title' => t('Pays'),
                '#required' => TRUE,
            );

            $form['email'] = array(
                '#type' => 'email',
                '#title' => t('Email'),
                '#required' => TRUE,
            );

            $form['birthday_date'] = array(
                '#type' => 'date',
                '#title' => t('Date'),
                '#required' => TRUE,
            );

            $form['cv'] = array(
                '#type' => 'file',
                '#title' => t('Déposer votre CV'),
            );

            $form['message'] = array(
                '#type' => 'textarea',
                '#title' => t('Message'),
                '#required' => TRUE,
            );

            $form['actions']['#type'] = 'actions';
            $form['actions']['submit'] = array(
                '#type' => 'submit',
                '#value' => $this->t('Envoyer'),
                '#button_type' => 'primary',
            );

            return $form;
        } else {
            drupal_set_message('Pas d\'offre disponible');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

      if ($form_state->getValue('id_offre') == null) {
        $form_state->setErrorByName('id_offre', $this->t('Vous devez choisir une offre à laquelle postuler.'));
      }

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $client = new \GuzzleHttp\Client();

        $data = ['data' => $form_state->getValues()];

        $data['data']['name'] = $form_state->getValue('lastname') . '_' . $form_state->getValue('firstname');

        $id = $form_state->getValue('id_offre');

        $response = $client->post('http://sandbox.skilvioo.com/api/applies/create/' . $id, [
            'json' => $data,
        ]);

        $response_body = json_decode($response->getBody(), true);

        $id_apply = $response_body['data']['id'];

        if(!empty($_FILES['files']['tmp_name']['cv'])){
            $client->post('http://sandbox.skilvioo.com/api/applies/upload-cv/' . $id_apply, [
                'multipart' => [
                    [
                        'name'     => 'file',
                        'contents' => fopen($_FILES['files']['tmp_name']['cv'], 'r')
                    ],
                ]
            ]);
        }
    }
}