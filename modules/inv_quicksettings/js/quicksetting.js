 jQuery(document).ready(function($){
    $('.quicksettings_toggle').click(function(){
      $('#block-quicksettings').toggleClass('open');
    })
    $('select.inv_layout').change(function(){
      $('body').removeClass('boxed wide').addClass($(this).val());
      $(window).trigger('resize');
    });
    if($('body').hasClass('boxed')){
      $('select.inv_layout').val('boxed').trigger('change');
    }else{
      $('select.inv_layout').val('wide').trigger('change');
    }
	 $('ul.inv_background span').click(function(){
      if($('select.inv_layout').val()=='wide'){
        alert('Please select boxed layout');
        return;
      }
      $('body').removeClass('bg-boxed-01 bg-boxed-02 bg-boxed-03 bg-boxed-04 bg-boxed-05 bg-boxed-06 bg-boxed-07').addClass($(this).attr('class'));
    })
	$('select.inv_direction').change(function(){
      $('body').removeClass('ltr rtl').addClass($(this).val());
    });
  });