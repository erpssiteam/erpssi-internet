<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvAnimationShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The tab shortcode.
 *
 * @Shortcode(
 *   id = "animation",
 *   title = @Translation("Animation"),
 *   description = @Translation("Create a animation")
 * )
 */
class InvAnimationShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
	  'type' => '',
      'delay' => '',	  
      'class' => '',
    ),
      $attributes
    );
	
    $classes = $this->addClass($attributes['class'], 'inv-animate');
	$output = [
      '#theme' => 'shortcode_animation',
      '#type' => $attributes['type'],
      '#delay' => $attributes['delay'],
      '#class' => $classes,
      '#content' => $text,
	  '#attached' => array(
            'library' => array('inv_block_custom/innovation_animation')
        )
    ];
	return drupal_render($output);
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[animation type="Animation Effect" delay="Time to delay" class="Additional class" link=""]text[/animation]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a animation  wrapper shortcode.
     Additional class names can be added by the <em>class</em> parameter.') . '</p>';
	}
    else {
      $output[] = $this->t('Inserts a animation shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
