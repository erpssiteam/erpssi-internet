<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvProgressShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The progress bar shortcode.
 *
 * @Shortcode(
 *   id = "progress",
 *   title = @Translation("Progress Bar"),
 *   description = @Translation("Create a progress bar")
 * )
 */
class InvProgressShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
	  'title' => '',
	  'icon' => '',
      'class' => '',
	  'percent' => '0'
    ),
      $attributes
    );

    $classes = $this->addClass($attributes['class'], 'inv-shortcode-progress');
    $output = [
      '#theme' => 'shortcode_progress',
      '#title' => $attributes['title'],
      '#icon' => $attributes['icon'],
      '#class' => $classes,
	  '#percent' => $attributes['percent'],
      '#content' => $text,
	  '#attached' => array(
            'library' => array('inv_shortcodes/shortcode.progress')
        )
    ];
	return drupal_render($output);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[progress class=" lass" title="" percent=""]text[/progress]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a progress bar shortcode.
    The <em>class</em> is a classes for customize row.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a progress bar shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
