<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvRowShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The icon shortcode.
 *
 * @Shortcode(
 *   id = "row",
 *   title = @Translation("Row"),
 *   description = @Translation("Create a row bootstrap")
 * )
 */
class InvRowShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
      'class' => '',
    ),
      $attributes
    );

    $classes = $this->addClass($attributes['class'], 'row');
    return "<div class='".$classes."'>".$text."</div>";
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[row class="Row class" link=""]text[/row]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a row bootstrap shortcode.
    The <em>class</em> is a classes for customize row.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a row bootstrap shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
