<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvTabsShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The carousels shortcode.
 *
 * @Shortcode(
 *   id = "carousel",
 *   title = @Translation("Carousel"),
 *   description = @Translation("Carousel Slide Item")
 * )
 */
class InvCarouselShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
        'path' => ''
    ),$attributes);
    $path_image = $attributes['path'];
    if (strpos($path_image, "public://") !== false) {
      $path_image = file_create_url($attributes['path']);
    }
    global $shortcode_carousel_stack;
    global $thumbnail;
    if (!is_array($shortcode_carousel_stack)) $shortcode_carousel_stack = array();
    $active = "";
    if (Count($shortcode_carousel_stack) == 0) $active = "active";
    $item = "<div class='item ".$active."'><img alt='' src='".$path_image."'/>".$text."</div>";
    $shortcode_carousel_stack[] = $item;
    $thumbnail[] = $path_image;
    return $item;
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[carousel path="PATH_IMAGE" link=""]text[/carousel]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a carousel item shortcode.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a carousel item shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
