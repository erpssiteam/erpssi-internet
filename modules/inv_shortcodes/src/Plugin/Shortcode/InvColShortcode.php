<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvColShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The column bootstrap shortcode.
 *
 * @Shortcode(
 *   id = "col",
 *   title = @Translation("Column"),
 *   description = @Translation("Create a column bootstrap shortcode")
 * )
 */
class InvColShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $attributes = $this->getAttributes(array(
      'class' => '',
      'xs' => '',
      'sm' => '',
      'md' => '',
      'lg' => '',
    ),
      $attributes
    );

    $class = $attributes['class'];
    foreach (['xs', 'sm', 'md', 'lg'] as $size) {
      if ($attributes[$size]) {
        $class = $this->addClass($class, 'col-' . $size . '-' . $attributes[$size]);
      }
    }
    return '<div class="' . $class . '">' . $text . '</div>';
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . t('[col class="custom-class" xs="12" sm="6" md="4" lg="3"]Other HTML content here [/col]') . '</strong> ';
    if ($long) {
      $output[] = t('Wraps your content with a div with bootstrap column size classes. All attributes are optional but it would not be very useful unless you define at least 1 size attribute or custom all the classes yourself using class. Setting md=4 translates to the col-md-4 class, etc.') . '</p>';
    }
    else {
      $output[] = t('Wraps your content with a div with bootstrap column size classes.') . '</p>';
    }

    return implode(' ', $output);
  }
}
