<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvEmbedBlockShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The embed block shortcode.
 *
 * @Shortcode(
 *   id = "embed",
 *   title = @Translation("Embed Block"),
 *   description = @Translation("Create an embed block")
 * )
 */
class InvEmbedBlockShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
      'block_id' => '',
    ),
      $attributes
    );

    $block = \Drupal\block\Entity\Block::load($attributes['block_id']);
	if (isset($block)) { 
		$block_content = \Drupal::entityManager()->getViewBuilder('block')->view($block);
		return drupal_render($block_content);
	}
	return "";
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[embed block_id="BlockID"]text[/embed]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts an embed block shortcode.
    The <em>block_id</em> is a blockId you want to embed. ') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts an embed block shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
