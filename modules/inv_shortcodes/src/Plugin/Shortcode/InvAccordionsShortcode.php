<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvStatShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;
use \Drupal\Core\Render\Markup;

/**
 * The accordions wrapper shortcode.
 *
 * @Shortcode(
 *   id = "accordions",
 *   title = @Translation("Accordions"),
 *   description = @Translation("Create a Accordions wrapper shortcode")
 * )
 */
class InvAccordionsShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $attributes = $this->getAttributes(array(
		'type'=>'accordion', // accordion or toggle
		'class' => '',
    ),
      $attributes
    );
    $wrapper_id = Html::getId('inv_accordions_wrapper_'. uniqid());
    if ($attributes['type'] == "toggle") {
      $text = str_replace('data-parent="#ACCORDION_WRAPPER_ID"', "", $text);
    } else {
      $text = str_replace('ACCORDION_WRAPPER_ID', $wrapper_id, $text);
    }
	$render_array = array(
        '#theme' => 'shortcode_accordions',
        '#accordion_wrapper_id' => $wrapper_id,
        '#type'=>$attributes['type'],
		'#class' => $attributes['class'],
        '#content' => ['#markup' => Markup::create($text)],
        '#attached' => array(
            'library' => array('inv_shortcodes/shortcode.stat')
        )
    );
    return drupal_render($render_array);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . t('[accordions type="" class="custom-class"][/accordions]') . '</strong> ';
    if ($long) {
      $output[] = t('Accordions shortcode') . '</p>';
    }
    else {
      $output[] = t('Create a accordions shortcode.') . '</p>';
    }

    return implode(' ', $output);
  }
}
