<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvMapShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The tab shortcode.
 *
 * @Shortcode(
 *   id = "map",
 *   title = @Translation("Map maker"),
 *   description = @Translation("Create a map maker")
 * )
 */
class InvMapShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
	    'latitude' => '',
        'longitude' => '',
		'link' => '',
        'title' => '',
		'phone' => '',
        'image' => '',
        'icon' => '',
    ),
      $attributes
    );

    return "<div class=\"inv-gmap-marker\" data-link=\"{$attributes['link']}\" data-title=\"{$attributes['title']}\" data-image=\"{$attributes['image']}\" data-phone=\"{$attributes['phone']}\" data-latitude=\"{$attributes['latitude']}\" data-longitude=\"{$attributes['longitude']}\" data-icon=\"{$attributes['icon']}\">{$text}</div>";
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
   $output[] = '<p><strong>' . $this->t('[map]text[/map]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a map maker. The <em>class</em> is a classes for customize map.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a map maker shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
