<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvStatShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;
use \Drupal\Core\Render\Markup;

/**
 * The accordion item shortcode.
 *
 * @Shortcode(
 *   id = "accordion",
 *   title = @Translation("Accordion"),
 *   description = @Translation("Create a Accordion item shortcode")
 * )
 */
class InvAccordionShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $item_id = Html::getId('inv_accordion_item_'. uniqid());
    $attributes = $this->getAttributes(array(
        'title'=>'', // accordion or toggle
        'icon' => '',
        'expand' => 'false'
    ),$attributes);

    $item_collapse = 'collapsed';
    $item_active = '';

    if ($attributes['expand'] === "true") {
      $item_active = 'in';
      $item_collapse = '';
    }
	$render_array = array(
        '#theme' => 'shortcode_accordion',
        '#accordion_item_id' => $item_id,
        '#item_collapse' => $item_collapse,
        '#item_active' => $item_active,
        '#title' => $attributes['title'],
        '#icon' => $attributes['icon'],
        '#content' => ['#markup' => Markup::create($text)],
    );
    return drupal_render($render_array);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . t('[accordion title="" icon="" expand=""][/accordion]') . '</strong> ';
    if ($long) {
      $output[] = t('Accordion item shortcode') . '</p>';
    }
    else {
      $output[] = t('Create a accordion item shortcode.') . '</p>';
    }

    return implode(' ', $output);
  }
}
