<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvTitleShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The column bootstrap shortcode.
 *
 * @Shortcode(
 *   id = "title",
 *   title = @Translation("Title"),
 *   description = @Translation("Create a title shortcode")
 * )
 */
class InvTitleShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $attributes = $this->getAttributes(array(
      'class' => '',
    ),
      $attributes
    );
	if (strpos($text, '~')) {
        $title_array = explode('~', $text);
        $main_title = $title_array[0];
        $sub_title = $title_array[1];
    } else {
        $main_title = $text;
		$sub_title = "";
    }
	$render_array = array(
        '#theme' => 'shortcode_title',
        '#class' => $attributes['class'],
		'#main_title' => $main_title,
        '#sub_title' => $sub_title,
    );
    return drupal_render($render_array);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . t('[title class="custom-class" maintitle="" subtitle=""][/title]') . '</strong> ';
    if ($long) {
      $output[] = t('Title shortcode') . '</p>';
    }
    else {
      $output[] = t('Create a title shortcode.') . '</p>';
    }

    return implode(' ', $output);
  }
}
