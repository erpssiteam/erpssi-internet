<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvIconShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The icon shortcode.
 *
 * @Shortcode(
 *   id = "icon",
 *   title = @Translation("Icon"),
 *   description = @Translation("Create an icon")
 * )
 */
class InvIconShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
      'class' => '',
      'link' => '',  
    ),
      $attributes
    );

    $classes = $this->addClass($attributes['class'], 'inv-icon');
    if ($attributes['link'] != "") {
		return "<a href='".$attributes['link']."' target='_blank'><i class='".$classes."'></i>".$text."</a>";
	}
    return "<i class='".$classes."'></i>".$text;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[icon class="Icon Class" link=""]text[/box]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a box shortcode.
    The <em>class</em> is a classes for icon such as fa fa-home.
    The <em>link</em> is a link when user click on icon') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts an icon shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
