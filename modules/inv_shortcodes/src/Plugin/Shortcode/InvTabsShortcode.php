<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvTabsShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The tab shortcode.
 *
 * @Shortcode(
 *   id = "tabs",
 *   title = @Translation("Tabs"),
 *   description = @Translation("Create a tab wrapper")
 * )
 */
class InvTabsShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
      'class' => '',
    ),
      $attributes
    );
	
    $classes = $this->addClass($attributes['class'], 'inv-tab-wrapper');
	$tab_wrapper_id = Html::getId('inv_tab_'. uniqid());

    global $shortcode_tabs_stack;
	$output = '<div id="'.$tab_wrapper_id.'" class="'.$classes.' clearfix"> <ul class="nav nav-tabs">';
    $tabCount = 1;
    $xclass = "";
    foreach ($shortcode_tabs_stack as $tab) {
        if ($tabCount == 1) {
            $output .= '<li class="active first"><a href="#'.$tab["id"].'" data-toggle="tab">
			<i class="tab-icon '.$tab["icon"].'"></i>'.$tab["title"].'</a></li>';
        } else {
            if ($tabCount == Count($shortcode_tabs_stack)) {
                $xclass="last";
            }
            $output .= '<li class="'.$xclass.'">
			<a href="#'.$tab["id"].'" data-toggle="tab"><i class="tab-icon '.$tab["icon"].'"></i>'.$tab["title"].'</a></li>';
        }
        $tabCount = $tabCount + 1;
    }
    $output .= '</ul>';
    $output .='<div class="tab-content">';

    $tabCount = 1;
    foreach ($shortcode_tabs_stack as $tab) {
        if ($tabCount == 1) {
            $output .= '<div id="'.$tab["id"].'" class="active tab-pane fade in">';
        } else {
            $output .= '<div id="'.$tab["id"].'" class="tab-pane fade in">';
        }
        $tabCount = $tabCount + 1;
        $output .= $tab["contents"]["#markup"].'</div>';
    }
    $output .='</div></div>';
    $shortcode_tabs_stack = null;
    return $output;
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[tabs class="Additional class" link=""]text[/tabs]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a tabs  wrapper shortcode.
     Additional class names can be added by the <em>class</em> parameter.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a tab shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
