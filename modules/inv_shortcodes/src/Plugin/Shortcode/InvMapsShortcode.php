<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvMapsShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The tab shortcode.
 *
 * @Shortcode(
 *   id = "maps",
 *   title = @Translation("Map wrapper"),
 *   description = @Translation("Create a map wrapper")
 * )
 */
class InvMapsShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
	    'class' => '',
        'height' => '400px',
        'type' => 'standard', //gray/color/standard
        'zoom' => '10',
        'key' => 'AIzaSyBDOx3gq6MFyzf6wPblrD5RohWqs8D7Htw', // Default key using for Innovation theme
    ),
      $attributes
    );
	$height = "height:".$attributes['height'];
	$classes = $this->addClass($attributes['class'], 'inv-gmap-shortcode');
	$element_attributes = [
      'class' => $classes,
      'id' => Html::getId('inv_gmap_shortcode'),
      'style' => $height,
      'data-type' => $attributes['type'],
	  'data-zoom' => $attributes['zoom'],
    ];

     $render_array = array(
        '#theme' => 'shortcode_maps',
        '#attributes' => $element_attributes,
        '#key' => $attributes['key'],
        '#content' => $text,
		'#attached' => array(
			'library' => array('inv_shortcodes/shortcode.map')
		)
    );
    return drupal_render($render_array);
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[maps ]text[/maps]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a map wrapper.
    The <em>title</em> is a box title.
    The <em>icon</em> can be FontAwesome class such as fa fa-home or any font which theme support.
    Additional class names can be added by the <em>class</em> parameter.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a maps wrapper') . '</p>';
    }
    return implode(' ', $output);
  }
}
