<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvStatShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The column bootstrap shortcode.
 *
 * @Shortcode(
 *   id = "stat",
 *   title = @Translation("Statitic"),
 *   description = @Translation("Create a Statitic shortcode")
 * )
 */
class InvStatShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $attributes = $this->getAttributes(array(
		'title' => '',
		'icon' => '',
		'number' => '',
		'start_number'=>'0',
		'inc_step'=>'1',
		'duration'=>'300', //ms
		'class' => '',
    ),
      $attributes
    );
	$render_array = array(
        '#theme' => 'shortcode_stat',
        '#stat_id'=> Html::getId('inv_stat_'. uniqid()),
        '#title' => $attributes['title'],
        '#icon' => $attributes['icon'],
		'#number' => $attributes['number'],
        '#start_number'=>$attributes['start_number'],
        '#inc_step'=>$attributes['inc_step'],
        '#duration'=>$attributes['duration'], //ms
		'#class' => $attributes['class'],
        '#content' => $text,
        '#attached' => array(
            'library' => array('inv_shortcodes/shortcode.stat')
        )
    );
    return drupal_render($render_array);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . t('[stat title="" icon="" class="custom-class" number="" start_number="" inc_step="" duration=""][/stat]') . '</strong> ';
    if ($long) {
      $output[] = t('Stat shortcode') . '</p>';
    }
    else {
      $output[] = t('Create a stat shortcode.') . '</p>';
    }

    return implode(' ', $output);
  }
}
