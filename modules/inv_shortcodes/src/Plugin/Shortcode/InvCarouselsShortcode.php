<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvTabsShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The carousels shortcode.
 *
 * @Shortcode(
 *   id = "carousels",
 *   title = @Translation("Carousels"),
 *   description = @Translation("Carousel wrapper")
 * )
 */
class InvCarouselsShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
      // Merge with default attributes.
      $attributes = $this->getAttributes(array(
          'carousel_id' => '',
          'pager' => 'control',
          'control' => 'true',
          'auto' => 'true',
          'content' => '',
          'thumbnail' => ''
        ),$attributes);

      $wrapper_id = Html::getId('inv_carousel_'. uniqid());
      global $shortcode_carousel_stack;
      global $thumbnail;
      $total = Count($shortcode_carousel_stack);
      $render_array = array(
          '#theme' => 'shortcode_carousels',
          '#carousel_id' => $wrapper_id,
          '#total_item' =>  $total,
          '#pager' => $attributes['pager'],
          '#control' => $attributes['control'],
          '#auto' => $attributes['auto'],
          '#content' =>  $text,
          '#thumbnail' => $thumbnail,
      );
      $shortcode_carousel_stack = null;
      $thumbnail = null;
      return drupal_render($render_array);
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[carousels class="Additional class" link=""]text[/carousels]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a carousels  wrapper shortcode.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a carousel wrapper shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
