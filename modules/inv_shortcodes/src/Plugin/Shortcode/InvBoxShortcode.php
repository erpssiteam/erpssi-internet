<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvBoxShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The box shortcode.
 *
 * @Shortcode(
 *   id = "box",
 *   title = @Translation("Box"),
 *   description = @Translation("Create a box with icon or image")
 * )
 */
class InvBoxShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
      'title' => '',
      'icon' => '',
      'image' => '',
      'number' => '',
      'border' => 'none',
      'align' => 'center',
	  'background' => 'no',
      'class' => '',
      'link' => '',
	  'link_text' => '',
      'type' => '',
    ),
      $attributes
    );

    $title = $this->getTitleFromAttributes($attributes['title'], $text);
    $classes = $this->addClass($attributes['class'], 'inv-box-shortcode');

	$path_image = $attributes['image'];
    if ($path_image != "") {
		if (strpos($path_image, "public://") !== false) {
			$path_image = file_create_url($path_image);
		}
        $classes = $this->addClass($classes, "box-image");
	}
    $classes = $this->addClass($classes, "box-".$attributes['border']);
    $classes = $this->addClass($classes, "box-".$attributes['align']);
    if ($attributes['type'] != "") {
        $classes = $this->addClass($classes, $attributes['type']);
    }
    if($attributes['background'] == 'yes'){
        $classes = $this->addClass($classes, "box-background");
    }
    $output = [
      '#theme' => 'shortcode_box',
      '#title' => $attributes['title'],
      '#icon' => $attributes['icon'],
      '#image' => $path_image,
      '#number' => $attributes['number'],
      '#class' => $classes,
      '#link' => $attributes['link'],
	  '#link_text' => $attributes['link_text'],
      '#type' => $attributes['type'],
      '#content' => $text,
    ];

    return $this->render($output);
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[box title="Tite" icon="" image="Path" border="Icon Border" align="Icon align" class="Additional class" link=""]text[/box]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a box shortcode.
    The <em>title</em> is a box title.
    The <em>icon</em> can be FontAwesome class such as fa fa-home or any font which theme support.
    The <em>image</em> is used when you want a image in box instead of icon.
    The <em>border</em> is a border of icon. It can be none, square, circle.
    The <em>align</em> is a icon align in box . It can be center, left, right (default is center).
    The <em>class</em> is a classes which you want to add to box wrapper. Classes split by space.
    The <em>link</em> is a link when user click on title of the box
    Additional class names can be added by the <em>class</em> parameter.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a box shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
