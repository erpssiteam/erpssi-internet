<?php

/**
 * @file
 * Contains \Drupal\inv_shortcodes\Plugin\Shortcode\InvTabShortcode.
 */

namespace Drupal\inv_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use \Drupal\Component\Utility\Html;

/**
 * The tab shortcode.
 *
 * @Shortcode(
 *   id = "tab",
 *   title = @Translation("Tab"),
 *   description = @Translation("Create a tab")
 * )
 */
class InvTabShortcode extends ShortcodeBase {
      /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes(array(
      'title' => '',
      'icon' => '',
      'class' => '',
    ),
      $attributes
    );

    $title = $this->getTitleFromAttributes($attributes['title'], $text);
    $classes = $this->addClass($attributes['class'], 'inv-box-shortcode');
	$tab_item_id = Html::getId('inv_tab_item_'. rand());
	global $shortcode_tabs_stack;

    if (!is_array($shortcode_tabs_stack)) $shortcode_tabs_stack = array();
    $tabPane = array(
        'title' => $title,
        'icon'  => $attributes['icon'],
        'id'    => $tab_item_id,
        'contents' => array(
            '#markup' => $text,
        ),
        'weight' => count($shortcode_tabs_stack),
    );
    $shortcode_tabs_stack[] = $tabPane;
  }


  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . $this->t('[tab title="Tite" icon="" class="Additional class" link=""]text[/tab]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Inserts a tab shortcode.
    The <em>title</em> is a box title.
    The <em>icon</em> can be FontAwesome class such as fa fa-home or any font which theme support.
    Additional class names can be added by the <em>class</em> parameter.') . '</p>';
    }
    else {
      $output[] = $this->t('Inserts a tab shortcode.') . '</p>';
    }
    return implode(' ', $output);
  }
}
